# README #
[]()

This Repo contains source code for an RTS prototype. The architecture is loosely based on MVC and depends on Zenject ( the Dependency Injection Library) which is included in the source
### Prototype Design ####
The Prototype is designed to be extendable in case new ideas need to be prototyped

### Code Structure ###

* Every Object in the world derives from WorldObject or its derivates 
* The World Object class controls the health of each object
* Objects that you can select are derived from SelectableWorldObject
* A loose composition based design is implemented using interfaces to give WorldObjects things such as the ability to follow commands and have weapons
* Objects utilize a regular Tick function instead of Unity's Update functions

### Controls ###

* Left Mouse Click to Select/DeSelect Unit
* Drag Left Mouse to Box Select
* Left Click on ground to clear selection
* Hold Left Shift to enable Additive Selection (Add units to current selection)
* Right click on the ground to move 
* Right click on an Enemy Player to Fire


### Author ###

* Muhammad Shibli Mansuri