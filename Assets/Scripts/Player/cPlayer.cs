﻿using UnityEngine;
using System.Collections;
using Zenject;
using System;

namespace RTS {
    public class cPlayer:IInitializable,IDisposable {
        PlayerUnitManager m_Units;
        cInput.LeftMouseClickedOrDragged m_Selection_Signal;
        cInput.RightMouseClicked m_Action_Signal;
        cInput.MutliSelect m_MultiSelect_Signal;
        bool m_AdditiveSelectionEnabled = false;
        Camera m_MainCamera;

        public cPlayer(PlayerUnitManager units, cInput.LeftMouseClickedOrDragged boxSelectSignal,cInput.MutliSelect multiSelectSig,
            cInput.RightMouseClicked actionSignal,[Inject(Id = GameInstaller.Cameras.Main)] Camera mainCamera) {
                m_Units = units;
                m_Selection_Signal = boxSelectSignal;
                m_Action_Signal = actionSignal;
                m_MultiSelect_Signal = multiSelectSig;
                m_MainCamera = mainCamera;
        }

        public void Initialize() {
            m_Action_Signal.Event += OnActionClicked;
            m_Selection_Signal.Event += OnMouseUp;
            m_MultiSelect_Signal.Event += OnAdditiveSelct;
        }

        public void Dispose() {
            m_Action_Signal.Event -= OnActionClicked;
            m_Selection_Signal.Event -= OnMouseUp;
            m_MultiSelect_Signal.Event -= OnAdditiveSelct;
        }

        void OnMouseUp(Vector2 startPoint, Vector2 deltaSize) {
            if(deltaSize==Vector2.zero) {
                m_Units.SingleSelectUnit(startPoint,m_AdditiveSelectionEnabled);
            }
            else {//this is a box select
                m_Units.BoxSelectUnits(startPoint,deltaSize,m_AdditiveSelectionEnabled);
            }
        }

        void OnActionClicked(Vector2 actionPoint) {
            
            if(m_Units.IsAnyUnitSelected) {
                Vector3 worldPoint;
                Ray ray = m_MainCamera.ScreenPointToRay(actionPoint);
                RaycastHit hit;
                LayerMask layerMask = LayerMask.GetMask("Ground","AIUnit");
                if(Physics.Raycast(ray,out hit,Mathf.Infinity,layerMask)) {
                    int layerHit = hit.transform.gameObject.layer;
                    if(layerHit == LayerMask.NameToLayer("Ground")) {
                        m_Units.SetSelectedUnitsDestination(hit.point);
                        Debug.Log("Set Destination point");
                    }
                        
                    else if(layerHit == LayerMask.NameToLayer("AIUnit")) {
                        IAIUnit targetObj = hit.transform.GetComponentInHierarchy<IAIUnit>();
                        
                       // Debug.Log("Found AI unit with health:" + targetObj.GetCurrentHealth());
                        m_Units.SetSlectedUnitsTarget(targetObj.SelectableWorldObject,hit.point);
                    }
                        

                }
                
            } 
        }

        void OnAdditiveSelct (bool enabled) {
            m_AdditiveSelectionEnabled = enabled;
        }

    }
}


