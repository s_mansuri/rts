﻿using UnityEngine;
using System.Collections;

namespace RTS {

    public static class UtilExtensions {
        public static Bounds GetViewPortBounds(this Camera camera,Vector3 screenPoint1, Vector3 screenPoint2) {
            Bounds toReturn = new Bounds();

            var v1 = camera.ScreenToViewportPoint(screenPoint1);
            var v2 = camera.ScreenToViewportPoint(screenPoint2);

            var min = Vector3.Min(v1,v2);
            var max = Vector3.Max(v1,v2);
            min.z = camera.nearClipPlane;
            max.z = camera.farClipPlane;

            toReturn.SetMinMax(min,max);
            return toReturn;
        }

        public static Color ToAlpha(this Color original, float alpha) {
            alpha = Mathf.Clamp01(alpha);
            return new Color(original.r,original.g,original.b,alpha);
        }

        public static Bounds GetTotalBounds(this WorldObject worldObj) {
            Bounds bounds = worldObj.GetComponentInChildren<MeshRenderer>().bounds;
            foreach(MeshRenderer mesh in worldObj.GetComponentsInChildren<MeshRenderer>())
                bounds.Encapsulate(mesh.bounds);
            return bounds;
        }

        public static T GetComponentInHierarchy<T>(this GameObject go)  {
            T result = go.GetComponentInChildren<T>();
            if(result == null)
                result = go.GetComponentInParent<T>();
            if(result == null)
                Debug.LogError("This object does not contain the component (" + result.ToString() + ")");
            return result;
        }

        public static T GetComponentInHierarchy<T>(this Transform transform)  {
            return transform.gameObject.GetComponentInHierarchy<T>();
        }

        public static float Map(this float x,float in_min,float in_max,float out_min,float out_max) {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
    }

}