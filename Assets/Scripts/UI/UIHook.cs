﻿using UnityEngine;
using System.Collections;

public class UIHook : MonoBehaviour {
    [SerializeField]
    RectTransform selectionBox;

    public void ResizeSelectionBox(Vector2 origin,Vector2 size) {
        selectionBox.anchoredPosition = origin;
        selectionBox.sizeDelta = size;
    }

}
