﻿using UnityEngine;
using System.Collections;
using Zenject;
using DG.Tweening;

namespace RTS {
    [RequireComponent(typeof(Rigidbody))]
    public class MovingProjectile : ProjectileBase {
        Rigidbody myRigidBody;
        [SerializeField]
        float moveSpeed = 30f;
        [Inject]
        void ConstructMovingProjectile() {
            Debug.Log("Moving Projectile Constructed");
            //m_velocity = 5f;
            myRigidBody = GetComponent<Rigidbody>();
            myTransform.position = Vector3.zero;


        }

        public override void MoveToTarget() {
            
            if(IsMoving) {
                //Debug.Log("Moving To " + GetTargetPosition());
                Vector3 targetPos = GetTargetPosition();
                targetPos.y = Transform.position.y;
                myRigidBody.DOMove(targetPos,moveSpeed).SetSpeedBased().SetEase(Ease.Linear).OnComplete(() => OnReachedTarget());
                IsMoving = false;
            }
            
        }
        
        void OnReachedTarget() {
           
            IsMoving = false;
             ReachedTarget = true;
           
        }

        Vector3 GetTargetPosition() {
            if(m_CurrentTarget != null)
                return m_CurrentTarget.Transform.position;
            else return m_TargetPosition;
        }

    }

}
