﻿using UnityEngine;
using System.Collections;
using System;

namespace RTS {
    
    public interface IWeapon  {
        IProjectile Projectile { get; }
        int Ammo { get; }
        IHealth CurrentTarget { get; set; }
        int ClipSize { get; }
        void UseWeapon(Vector3 worldPosition);
        void UseWeapon(IHealth target);
        void Reload();
        bool CanShoot();

        void Tick();
        void FixedTick();
        Transform Transform { get; }
    }
}

