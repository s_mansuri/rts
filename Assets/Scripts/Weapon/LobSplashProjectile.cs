﻿using UnityEngine;
using System.Collections;
using Zenject;
using DG.Tweening;
using System.Collections.Generic;

namespace RTS {
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(SphereCollider))]
    public class LobSplashProjectile : ProjectileBase {

        Rigidbody myRigidBody;
        [SerializeField]
        float m_moveSpeed = 30f;
        [SerializeField]
        float m_AverageHeight = 5f;
        [SerializeField]
        float m_SplashRadius = 3.9f;

        SphereCollider myCollider;

        List<Transform> m_PotentialTargets;
        bool m_Lobbed = false;

        [Inject]
        void ConstructLobSplashProjectile() {
            Debug.Log("LobSplash Projectile Constructed");
            m_velocity = 5f;
            myRigidBody = GetComponent<Rigidbody>();
            myTransform.position = Vector3.zero;

            myCollider = GetComponent<SphereCollider>();
            myCollider.radius = m_SplashRadius;
            InitializeProjectile();
            
        }
        void InitializeProjectile() {
            m_Lobbed = false;
            if(m_PotentialTargets == null)
                m_PotentialTargets = new List<Transform>();
            else
                m_PotentialTargets.Clear();
        }


        public override void MoveToTarget() {
            InitializeProjectile();
            if(IsMoving && !m_Lobbed) {
                Debug.Log("Jumping To " + GetTargetPosition());
                LobProjectile(GetTargetPosition());
                //Vector3 targetPos = GetTargetPosition();
                //targetPos.y = Transform.position.y;
                //myRigidBody.DOJump(targetPos, m_AverageHeight,0,m_moveSpeed).SetSpeedBased().SetEase(Ease.Linear).OnComplete(() => OnReachedTarget());
                //IsMoving = false;
                m_Lobbed = true;
            }
            else {
                if(Vector3.Distance(myTransform.position,GetTargetPosition()) < 0.1)
                    OnReachedTarget();
            }

        }

        void LobProjectile(Vector3 targetPosition,float initialAngle = 45f) {
            var rigid = GetComponent<Rigidbody>();

            

            float gravity = Physics.gravity.magnitude;
            // Selected angle in radians
            float angle = initialAngle * Mathf.Deg2Rad;

            // Positions of this object and the target on the same plane
            Vector3 planarTarget = new Vector3(targetPosition.x,0,targetPosition.z);
            Vector3 planarPostion = new Vector3(transform.position.x,0,transform.position.z);

            // Planar distance between objects
            float distance = Vector3.Distance(planarTarget,planarPostion);
            // Distance along the y axis between objects
            float yOffset = transform.position.y - targetPosition.y;

            float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance,2)) / (distance * Mathf.Tan(angle) + yOffset));

            Vector3 velocity = new Vector3(0,initialVelocity * Mathf.Sin(angle),initialVelocity * Mathf.Cos(angle));

            // Rotate our velocity to match the direction between the two objects
            float angleBetweenObjects = Vector3.Angle(Vector3.forward,planarTarget - planarPostion);
            Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects,Vector3.up) * velocity;

            // Fire!
            rigid.velocity = finalVelocity;

            // Alternative way:
            // rigid.AddForce(finalVelocity * rigid.mass, ForceMode.Impulse);
        }


        void OnReachedTarget() {

            IsMoving = false;
            ReachedTarget = true;

        }

        public override void DoDamage() {
            foreach(Transform obj in m_PotentialTargets) {
                IHealth healthObj = obj.GetComponentInHierarchy<IHealth>();
                if(healthObj!= null) {
                    if(healthObj is AIUnit) {
                        float distanceAttenuation = Vector3.Distance(myTransform.position,healthObj.Transform.position) / m_SplashRadius;
                        healthObj.TakeDamage(m_MaxDamage * m_SplashRadius);
                    }
                }
            }

            DamageDone = true;
            base.Deactivate();
            
        }

        Vector3 GetTargetPosition() {
            if(m_CurrentTarget != null)
                return m_CurrentTarget.Transform.position;
            else return m_TargetPosition;
        }

        public void OnTriggerEnter(Collider other) {
            m_PotentialTargets.Add(other.transform);
        }

        public void OnTriggerExit(Collider other) {
            m_PotentialTargets.Remove(other.transform);
        }
    }
}

