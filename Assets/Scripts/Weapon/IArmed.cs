﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public interface IArmed {
      
        IWeapon Weapon { get; set; }
    }
}

