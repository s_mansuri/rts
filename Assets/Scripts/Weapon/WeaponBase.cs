﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;
using System.Collections.Generic;

namespace RTS {
    [RequireComponent(typeof(AudioSource))]
    public class WeaponBase : MonoBehaviour, IWeapon {
        [SerializeField]
        protected int m_Ammo;
        [SerializeField]
        protected int m_ClipSize;
        [SerializeField]
        protected IHealth m_CurrentTargetClass;
        [SerializeField]
        GameObject m_ProjectilePrefab;
        [SerializeField]
        protected float m_RateOfFire;
        [SerializeField]
        protected float m_ReloadTime;
        
        protected int m_CurrentClip;
       
        protected float m_CoolDownTime;

        protected float m_CurrentReloadTime;
        
        protected Vector3 m_CurrentTargetPosition;

        List<IProjectile> m_UsableProjectiles;
        List<IProjectile> m_UnUsableProjectiles;
        protected DiContainer m_Container;
        Transform myTransform;
        public AudioSource m_AudioSource;
        public AudioClip m_ShotClip;

        void Awake() {
            
        }

        [Inject]
        void ConstructBaseWeapon(DiContainer container) {
            //Debug.Log(this + " is getting constructed");
           
            m_Container = container;
            myTransform = this.transform;
            CreateProjectilePool();

        }

        protected virtual void CreateProjectilePool(){
            if(m_ProjectilePrefab == null) {
                Debug.LogError("This Weapon needs Projectiles");
                return;
            }

            m_UsableProjectiles = new List<IProjectile>();
            m_UnUsableProjectiles = new List<IProjectile>();
            //m_Projectile = InstantiateNewProjectile();
            //m_UsableProjectiles.Add(m_Projectile);
        }

        
       

        public virtual void UseWeapon(Vector3 worldPosition) {
            if(CanShoot()) {
                m_CurrentTargetClass = null;
                m_CurrentTargetPosition = worldPosition;
                ShootProjectile();
            }
        }

        public virtual void UseWeapon(IHealth target) {
            if(CanShoot()) {
                m_CurrentTargetClass = target;
                ShootProjectile();
            }
        }

        protected virtual void ShootProjectile() {
            Debug.Log("Projectile has been shot");
            m_CoolDownTime = 0;
            m_CurrentClip--;
            //m_Ammo--;

            var newProj = GetNewProjectileFromPool();
            newProj.Transform.rotation = Transform.rotation;
            if(m_CurrentTargetClass == null) {
                newProj.ShootAtGround(m_CurrentTargetPosition);
            }
            else {
                newProj.ShootAtTarget(m_CurrentTargetClass);
            }

            m_AudioSource.clip = m_ShotClip;
            m_AudioSource.Play();
            
        }

        public virtual void Tick() {
            if(StillReloading)
                m_CurrentReloadTime += Time.deltaTime;
            if(OnCoolDown)
                m_CoolDownTime += Time.deltaTime;
            if(ShouldReload)
                Reload();

            
        }

        public virtual void FixedTick() {
            //move or reuse projectiles
            if(m_UnUsableProjectiles.Count > 0) {
                List<IProjectile> toReUse = new List<IProjectile>();
                foreach(IProjectile projectile in m_UnUsableProjectiles) {

                    if(projectile.IsUsable()) {
                        toReUse.Add(projectile);
                    }
                    else {
                        projectile.Tick();
                    }
                }

                if(toReUse.Count > 0) {
                    foreach(IProjectile proj in toReUse) {
                        m_UnUsableProjectiles.Remove(proj);
                        m_UsableProjectiles.Add(proj);
                    }
                }
            }
        }
        public virtual void Reload() {

            if(!StillReloading&&!ClipFull) {
                if(ClipEmpty)
                    m_CurrentReloadTime = 0f;
                int amountToAdd = m_ClipSize - m_CurrentClip;
                amountToAdd = (m_Ammo - amountToAdd) < 0 ? m_Ammo : amountToAdd;
                m_CurrentClip += amountToAdd;
                m_Ammo -= amountToAdd;
                
            }
            
        }

        public virtual bool CanShoot() {
            if(!OnCoolDown && !StillReloading) {
                if(ClipEmpty) {
                    Reload();
                    return false;//should reload
                }
                else if(HaveAmmo)
                    return true;
                else
                    return false;//no ammo
            }
            else {
                return false;//reloading or on cooldown
            }
        }

        IProjectile GetNewProjectileFromPool() {
            if(m_ProjectilePrefab == null) {//this weapon doesn't have projectiles
                Debug.Log("THis weapon doesn't have projectiles");
                return null;
            }
            
            else {
                IProjectile toReturn;
                if(m_UsableProjectiles.Count > 0) {
                    toReturn = m_UsableProjectiles[0];
                    toReturn.GameObject.SetActive(true);
                    m_UsableProjectiles.RemoveAt(0);
                }
                else {
                    toReturn = InstantiateNewProjectile();
                    
                }

                m_UnUsableProjectiles.Add(toReturn);
                return toReturn;
            }
           
        }


        IProjectile InstantiateNewProjectile() {
            var toReturn = m_Container.InstantiatePrefab(m_ProjectilePrefab).GetComponent<IProjectile>();
            toReturn.Transform.parent = this.Transform;
            toReturn.Transform.localPosition = Vector3.zero;
            return toReturn;
        }

        bool OnCoolDown { get { return m_CoolDownTime < m_RateOfFire; } }
        bool StillReloading { get { return m_CurrentReloadTime < m_ReloadTime; } }
        bool HaveAmmo { get { return m_Ammo!=0; } }
        bool ClipEmpty {get { return m_CurrentClip==0 ; } }
        bool ShouldReload { get { return ClipEmpty && !StillReloading; } }
        bool ClipFull { get { return m_CurrentClip == m_ClipSize; } }

        public IProjectile Projectile {
            get {
                return m_ProjectilePrefab.GetComponentInHierarchy<IProjectile>();
            }
        }

        public int Ammo {
            get {
                return m_Ammo;
            }
        }

        public IHealth CurrentTarget {
            get {
                return m_CurrentTargetClass;
            }
            set {
                m_CurrentTargetClass = value;
            }
        }

        public int ClipSize {
            get {
                return m_ClipSize;
            }
        }

        public Transform Transform {
            get {
                return myTransform;
            }
        }
    }
}

