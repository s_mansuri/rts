﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;

namespace RTS {
    public class ProjectileBase :MonoBehaviour, IProjectile {
        [SerializeField]
        protected float m_velocity;
        [SerializeField]
        protected float m_Damage;
        [SerializeField]
        protected float m_MaxDamage;
        [SerializeField]
        protected float m_Range;

        protected IHealth m_CurrentTarget;
        protected Vector3 m_TargetPosition;

        protected GameObject myGameObject;
        protected Transform myTransform;

        bool m_Moving = false;
        bool m_ReachedTarget = false;
        bool m_DamageDone = false;
        bool m_Deactivated = false;

        void Awake() {
           

        }

        [Inject]
        void ConstructProjectile() {
            Debug.Log("Projectile has been constructed");
            myTransform = this.transform;
            myGameObject = this.gameObject;
            InitializeProjectile();
        }

        void InitializeProjectile() {
            m_Moving = false;
            m_ReachedTarget = false;
            m_DamageDone = false;
            m_Deactivated = false;
        }

        public void ShootAtGround(Vector3 worldPosition) {
            if(!IsMoving) {
                InitializeProjectile();
                m_TargetPosition = worldPosition;
                IsMoving = true;
            }
            else PrintError(" Already Moving");
        }

        public void ShootAtTarget(IHealth target = null) {
            if(target != null)
                m_CurrentTarget = target;
            if(m_CurrentTarget != null && !IsMoving) {
                InitializeProjectile();
                IsMoving = true;
            }
            else PrintError("Either Moving Or No target");
                
        }

        public virtual void Tick() {
            if(!m_Deactivated) {
                if(IsMoving)
                    MoveToTarget();
                if(ReachedTarget && !m_DamageDone)
                    DoDamage();
                if(m_DamageDone)
                    Deactivate();
            }
            
            
        }

        public virtual void MoveToTarget() {
            Debug.Log("Static Move to Target");
            IsMoving = false;
            ReachedTarget = true;
        }


        public virtual void DoDamage() {
            if(m_CurrentTarget != null) {

                m_CurrentTarget.TakeDamage(Damage);
                Debug.Log("Damaged Target , new Health is:" + m_CurrentTarget.GetCurrentHealth());
                m_DamageDone = true;
                Deactivate();
            }
                
            
        }

        protected void Deactivate() {
            this.GameObject.SetActive(false);
            m_Deactivated = true;
        }

        public bool IsMoving { get { return m_Moving; }set { m_Moving = true; } }
        public bool ReachedTarget { get { return m_ReachedTarget; } protected set { m_ReachedTarget = value; } }
        public bool DamageDone { get { return m_DamageDone; }protected set { m_DamageDone = value; } }
        public float Damage {
            get {
                return m_Damage;
            }
        }

        public float MaxDamage {
            get {
                return m_MaxDamage;
            }
        }

        public IHealth Target {
            get {
                return m_CurrentTarget;
            }

            set {
                m_CurrentTarget = value;
            }
        }

        public float Velocity {
            get {
                return m_velocity;
            }
        }

        public float Range {
            get {
                return m_Range;
            }
        }

        public GameObject GameObject {
            get {
                return myGameObject;
            }
        }

        public Transform Transform {
            get {
                return myTransform;
            }
        }

        void PrintError(string message= "") {
            Debug.LogError("Projectile Error: " +message);
        }

        public bool IsUsable() {
            if(!IsMoving && ReachedTarget && m_DamageDone && m_Deactivated)
                return true;
            else return false;
        }
    }
}

