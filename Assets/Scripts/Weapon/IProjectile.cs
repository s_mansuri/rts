﻿using UnityEngine;
using System.Collections;
namespace RTS {
    public interface IProjectile {
        float Velocity { get; }
        float Damage { get; }
        float MaxDamage { get; }
        float Range { get; }
        IHealth Target{ get; set; }
        void ShootAtGround(Vector3 worldPosition);
        void ShootAtTarget(IHealth target);
        void DoDamage();
        void MoveToTarget();
        void Tick();
        bool IsUsable();
        GameObject GameObject { get; }
        Transform Transform { get; }
    }
}

