﻿using UnityEngine;
using System.Collections;
using Zenject;
using System;

namespace RTS {

    public class cInput : ITickable {

        Camera _mainCamera;
        UIHook _uiHook;

        Vector2 m_InitialClickPosition = Vector2.zero;
        Vector2 m_StartPoint;
        Vector2 m_Difference;

        LeftMouseClickedOrDragged.Trigger m_SelectionTrigger;
        RightMouseClicked.Trigger m_ActionTrigger;
        MutliSelect.Trigger m_MultiSelectTrigger;

        public cInput([Inject(Id = GameInstaller.Cameras.Main)] Camera mainCamera,
        UIHook uiHook,
        LeftMouseClickedOrDragged.Trigger selectionTrig,
        RightMouseClicked.Trigger rightMouseTrigger,
        MutliSelect.Trigger multipleSelectionTrigger) {
            _mainCamera = mainCamera;
            _uiHook = uiHook;
            m_SelectionTrigger = selectionTrig;
            m_ActionTrigger = rightMouseTrigger;
            m_MultiSelectTrigger = multipleSelectionTrigger;
        }
    
        public void Tick() {

            if(Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();

            if(Input.GetMouseButtonDown(0)) {
                m_InitialClickPosition = Input.mousePosition;
                _uiHook.ResizeSelectionBox(m_InitialClickPosition,Vector2.zero);
                m_StartPoint = m_InitialClickPosition;
                
            }
            if(Input.GetMouseButton(0)) {
                m_Difference = (Vector2)Input.mousePosition - m_InitialClickPosition;

                m_StartPoint = m_InitialClickPosition;
                if(m_Difference.x < 0) {//dragging left to right
                    m_StartPoint.x = Input.mousePosition.x;
                    m_Difference.x *= -1;
                }
                if(m_Difference.y < 0){//dragging bottom to top
                    m_StartPoint.y = Input.mousePosition.y;
                    m_Difference.y *= -1;
                }
                _uiHook.ResizeSelectionBox(m_StartPoint,m_Difference);
            }
            if(Input.GetMouseButtonUp(0)) {
                m_SelectionTrigger.Fire(m_StartPoint,m_Difference);
                m_InitialClickPosition = Vector2.zero;
                m_StartPoint = Vector2.zero;
                m_Difference = Vector2.zero;
                _uiHook.ResizeSelectionBox(m_StartPoint,m_Difference);
            }

            if (Input.GetMouseButtonDown(1)){
                //Debug.Log("Right Click");
                m_ActionTrigger.Fire(Input.mousePosition);
            }

            if(Input.GetKeyDown(KeyCode.LeftShift)) {
                m_MultiSelectTrigger.Fire(true);
            }
            if(Input.GetKeyUp(KeyCode.LeftShift)) {
                m_MultiSelectTrigger.Fire(false);
            }
        }


        void FindSelectionScreenPoint(Vector2 point) {
            Ray ray = _mainCamera.ScreenPointToRay(point);
            RaycastHit hit;
            if(Physics.Raycast(ray,out hit,Mathf.Infinity)) {
                Debug.Log(hit.collider.transform.root.name);
            }
        }

        public class LeftMouseClickedOrDragged : Signal<Vector2,Vector2> {
            public class Trigger : TriggerBase { }
        }

        public class RightMouseClicked : Signal<Vector2> {
            public class Trigger : TriggerBase { }
        }

        public class MutliSelect:Signal<bool> {
            public class Trigger : TriggerBase { }
        }
    }
}

