﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;

namespace RTS {
    [RequireComponent(typeof(AudioSource))]
    public class WorldObject : MonoBehaviour,IHealth {

        [SerializeField]
        protected string m_ObjectName = "";

        protected cHealth m_Health;
        protected Transform myTransform;
        protected Camera m_MainCamera;
        Transform m_CameraTransform;

        public AudioSource m_AudioSource;
        public AudioClip m_Clip;
        [Inject]
        public virtual void Initialize([Inject(Id = GameInstaller.Cameras.Main)] Camera mainCam) {
            m_Health = new cHealth(transform,100f);
            m_MainCamera = mainCam;
            this.AlignHealthBar(true);
            //Debug.Log("Zenject called WOrldObject");
            myTransform = this.transform;
            m_CameraTransform = mainCam.transform;
        }

        
        
        public virtual void Tick() {
            if(!this.IsAlive())
                HandleDeath();
            else {
                m_Health.Tick();
            }
            
        }

        public virtual void HandleDeath() {
            Debug.Log(m_ObjectName + " is dead");
            AudioSource source = m_MainCamera.GetComponent<AudioSource>();
            source.clip= m_Clip;
            source.Play();
        }

        public void EnableDisplayables() {
            this.EnableHealthBar();
        }

        public void DisableDisplayables() {
            this.DisableHealthBar();
        }
        public cHealth Health {
            get {
                return m_Health;
            }
        }

        public virtual string ObjectName {
            get {
                return m_ObjectName;
            }
            set {
                m_ObjectName = value;
            }
        }

        protected Quaternion MainCameraRotation {
            get {
                return m_CameraTransform.rotation;
            }
        }

        public Transform Transform {
            get {
                return myTransform;
            }
        }

        public Bounds GetWorldBounds() {
            return this.GetTotalBounds();
        }

        public void DestroyObject() {
            Destroy(this);
        }
    }
}

