﻿using UnityEngine;
using System.Collections;
using Zenject;
using System.Collections.Generic;
using System;

namespace RTS {
    public class WorldObjectsManager:IInitializable ,ITickable,IDisposable {

        List<ISelectable> m_AllSelectableUnits;
        List<IAIUnit> m_AllAIUnits;
        List<IPlayerUnit> m_AllPlayerUnits;
        PlayerUnit.PlayerUnitDeathSignal m_PlayerUnitDeath_SIG;
        AIUnit.AIUnitDeathSignal m_AIUnitDeath_SIG;

        List<IAIUnit> m_AIUnitsToDestroy;
        List<IPlayerUnit> m_PlayerUnitsToDestroy;

        public WorldObjectsManager(List<ISelectable> selectables, PlayerUnit.PlayerUnitDeathSignal playerDeathSig, AIUnit.AIUnitDeathSignal aiUnitDeathSig) {
            
            m_AllSelectableUnits = selectables;
            m_PlayerUnitDeath_SIG = playerDeathSig;
            m_AIUnitDeath_SIG = aiUnitDeathSig;

        }

        public void Initialize() {
            m_AllAIUnits = new List<IAIUnit>();
            m_AllPlayerUnits = new List<IPlayerUnit>();
            DivideUnits();
            Debug.Log("World Objects Initialized");
            m_PlayerUnitDeath_SIG.Event += RemovePlayerUnitFromWorld;
            m_AIUnitDeath_SIG.Event += RemoveAIUnitFromWorld;
        }

        void DivideUnits() {
            foreach(ISelectable unit in m_AllSelectableUnits) {
                if(unit is IPlayerUnit)
                    m_AllPlayerUnits.Add(unit as IPlayerUnit);
                else if(unit is IAIUnit)
                    m_AllAIUnits.Add(unit as IAIUnit);
            }

        }

        public void Tick() {
            for(int i = m_AllAIUnits.Count - 1;i >= 0;i--) {
                IAIUnit unit = m_AllAIUnits[i];
                if(unit.SelectableWorldObject.gameObject.activeInHierarchy)
                    unit.Tick();
                else {
                    m_AllAIUnits.RemoveAt(i);
                    unit.SelectableWorldObject.DestroyObject();
                }
            }
            
        }

        public List<IPlayerUnit> AllPlayerUnitsList {
            get {
                return m_AllPlayerUnits;
            }
        }
       
        public void RemovePlayerUnitFromWorld(IPlayerUnit unit) {
            m_AllPlayerUnits.Remove(unit);
            m_AllSelectableUnits.Remove(unit.Selectable);
            unit.Selectable.DestroyObject();
        }

        void RemoveAIUnitFromWorld(IAIUnit unit) {
            // m_AllAIUnits.Remove(unit);
            //m_AllSelectableUnits.Remove(unit.SelectableWorldObject);
            //unit.SelectableWorldObject.DestroyObject();
            unit.SelectableWorldObject.gameObject.SetActive(false);

        }

        public void Dispose() {
            m_PlayerUnitDeath_SIG.Event -= RemovePlayerUnitFromWorld;
            m_AIUnitDeath_SIG.Event -= RemoveAIUnitFromWorld;
        }
    }
}


