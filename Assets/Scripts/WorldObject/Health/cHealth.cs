﻿using UnityEngine;
using System.Collections;
namespace RTS {

    public class cHealth {

        float m_MaxHealth = 100;
        public float m_CurrentHealth;
        vHealth m_HealthView;

        public cHealth(Transform healthBarParent = null,float maxHealth = -1) {
            if(maxHealth != -1) {
                m_MaxHealth = maxHealth;
            }
            m_CurrentHealth = maxHealth;

            if(null != healthBarParent) {
                m_HealthView = new vHealth(healthBarParent);
                UpdateDisplay();
            }
        }
        public void Tick() {
            if(m_CurrentHealth <= m_MaxHealth * 0.4f) {//if at low Health
                m_HealthView.ForceDisplayToAlwaysOn(true);

            }else {
                m_HealthView.ForceDisplayToAlwaysOn(false);
            }
            m_HealthView.Tick();
        }

        public float GetHealth() {
            return m_CurrentHealth;
        }

        public float GetMaxHealth() {
            return m_MaxHealth;
        }

        public float TakeDamage(float amount) {
            m_CurrentHealth -= amount;
            //EnableDisplay(true);
            m_HealthView.FlashDisplay(2f);
            UpdateDisplay();
            return m_CurrentHealth;
        }

        public float Heal(float amount) {
            m_CurrentHealth = Mathf.Min(m_CurrentHealth + amount,m_MaxHealth);
            UpdateDisplay();
            return m_CurrentHealth;
        }

        public bool IsAlive() {
            return m_CurrentHealth > 0;
        }

        public void UpdateDisplay() {
            m_HealthView.UpdateDisplay(m_CurrentHealth / m_MaxHealth);
        }

        public void AlignHealthBarToCamera(bool instantly = false) {
            m_HealthView.AlignToCamera(instantly);
        }

        public void EnableViewDueToSelection() { m_HealthView.IsSelected = true; }
        public void DisableVIewDueToDeSelection() { m_HealthView.IsSelected = false; }


    }

}