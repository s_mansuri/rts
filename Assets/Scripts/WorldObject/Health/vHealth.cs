﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;

namespace RTS {
    public class vHealth {
        float m_MaxWidth = 3f;
        float m_MaxHeight = 0.5f;
        float m_VertOffset = 1f;

        Canvas m_Canvas;
        Image m_HealthBarImage;
        RectTransform m_CanvasRectTransform;
        RectTransform m_HealthBarImageRectTransform;
        Transform parentTransform;
        bool m_AlwaysOn = false;
        bool m_Selected = false;
        float m_FlashStartTime = 0f;
        float m_TotalFlashDuration = 0f;

        public vHealth(Transform parent = null) {
            if(parent == null) {
                Debug.LogError("Health Bar has no parent");
                return;
            }
            parentTransform = parent;
            CreateWorldSpaceCanvas(parent);
            m_FlashStartTime = 0f;
            m_TotalFlashDuration = 0f;
        }

        void CreateWorldSpaceCanvas(Transform parent) {
            m_Canvas =  new GameObject("HealthBarCanvas").AddComponent<Canvas>();
            m_HealthBarImage = new GameObject("Image").AddComponent<Image>();
            m_Canvas.renderMode = RenderMode.WorldSpace;
            m_Canvas.transform.parent = parent.transform;
            
            m_HealthBarImage.rectTransform.parent = m_Canvas.transform;

            if(m_Canvas.transform as RectTransform) {
                m_CanvasRectTransform = m_Canvas.transform as RectTransform;
                m_CanvasRectTransform.anchorMin = Vector2.zero;
                m_CanvasRectTransform.anchorMax = Vector2.zero;

                float modelHeight = GetParentMeshHeight(parent);
                m_CanvasRectTransform.anchoredPosition3D = new Vector3(0f,modelHeight+m_VertOffset,0f);
                m_CanvasRectTransform.pivot = new Vector2(0.5f,0f);
                m_CanvasRectTransform.sizeDelta = new Vector2(m_MaxWidth,m_MaxHeight);
            }
            if(m_HealthBarImage.transform as RectTransform) {
                m_HealthBarImageRectTransform = m_HealthBarImage.transform as RectTransform;
                m_HealthBarImageRectTransform.anchorMin = Vector2.zero; //new Vector2(0.5f,0f);
                m_HealthBarImageRectTransform.anchorMax = Vector2.zero; //new Vector2(0.5f,0f);
                m_HealthBarImageRectTransform.anchoredPosition3D = Vector3.zero;
                m_HealthBarImageRectTransform.pivot = Vector2.zero;
                m_HealthBarImageRectTransform.sizeDelta = new Vector2(m_MaxWidth,m_MaxHeight);
            }

        }

        private float GetParentMeshHeight(Transform parent) {
            var totalBounds = parent.GetComponentInChildren<MeshRenderer>().bounds;
            var meshRenderers = parent.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer mesh in meshRenderers) totalBounds.Encapsulate(mesh.bounds);
            return totalBounds.size.y;
        }

        public void UpdateDisplay(float percentageHealth) {
            if(!m_Canvas.enabled)
                return;
            percentageHealth = Mathf.Clamp01(percentageHealth);
            m_HealthBarImageRectTransform.sizeDelta = new Vector2(m_MaxWidth * percentageHealth,m_MaxHeight);

            m_HealthBarImage.color = GetColorFromHealth(percentageHealth);

            
        }

        Color GetColorFromHealth(float percentageHealth) {
            Color toReturn = Color.white;
            float toAlpha = (1 - percentageHealth).Map(0f,1f,0.5f,1f);
            if(percentageHealth >= 0.5f)
                toReturn = Color.Lerp(Color.yellow.ToAlpha(toAlpha),Color.green.ToAlpha(toAlpha),percentageHealth.Map(0.5f,1f,0f,1f));
            else if(percentageHealth<0.5f)
                toReturn = Color.Lerp(Color.red.ToAlpha(toAlpha),Color.yellow.ToAlpha(toAlpha),percentageHealth.Map(0.0f,0.5f,0f,1f));
            return toReturn;

        }

        public void Tick() {
            if(FlashIsRunning() && ShouldEndFlash()) {
                m_FlashStartTime=0;
                m_TotalFlashDuration = 0;
                DisableDisplay();
            }
            if(ShouldEnable()&& !m_Canvas.isActiveAndEnabled)
                EnableDisplay();
            else if(CanDisable() && m_Canvas.isActiveAndEnabled)
                DisableDisplay();
        }
        public void EnableDisplay() {
            //Debug.Log((enable)?"Enabling Display":"Disabling Display");
            if(m_Canvas.isActiveAndEnabled != true)
                m_Canvas.enabled = true;
        }

        public void DisableDisplay() {
            if(CanDisable() && m_Canvas.isActiveAndEnabled == true)
                m_Canvas.enabled = false;
        }

        public void FlashDisplay(float duration) {
            m_TotalFlashDuration += duration;
            if(!FlashIsRunning())
                m_FlashStartTime = Time.time;
            EnableDisplay();
        }

        public void AlignToCamera(bool instantly = false) {
            
            m_CanvasRectTransform.DOLookAt(Camera.main.transform.position,instantly?0f:0.2f,AxisConstraint.None,Camera.main.transform.up*-1);
            
            
        }
        bool FlashIsRunning() { return m_FlashStartTime != 0; }
        bool ShouldEndFlash() { return Time.time - m_FlashStartTime >= m_TotalFlashDuration; }
        bool CanDisable() { return !FlashIsRunning()&& !m_AlwaysOn&&!IsSelected; }
        bool ShouldEnable() { return !CanDisable(); }
        public bool IsSelected { get { return m_Selected; } set { m_Selected = value; } }
        public void ForceDisplayToAlwaysOn(bool alwaysOn = false) {
            m_AlwaysOn = alwaysOn;
            
        }
    }
}
