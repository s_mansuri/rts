﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public interface IHealth:IDisplayable {

        cHealth Health { get; }
        Transform Transform { get; }
    }
}

