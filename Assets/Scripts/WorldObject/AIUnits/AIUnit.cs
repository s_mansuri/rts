﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;

namespace RTS {
    public class AIUnit : SelectableWorldObject, IAIUnit {
        AIUnitDeathSignal.Trigger m_DeathTrigger;
        [Inject]
        void ConstructAIUnit(AIUnitDeathSignal.Trigger deathTrigger) {
            m_DeathTrigger = deathTrigger;
        }

        public override void HandleDeath() {
            base.HandleDeath();
            m_DeathTrigger.Fire(this);
        }
        public SelectableWorldObject SelectableWorldObject {
            get {
                return this;
            }
        }

        public class AIUnitDeathSignal : Signal<IAIUnit>{
            public class Trigger : TriggerBase { }
        }


    }
}

