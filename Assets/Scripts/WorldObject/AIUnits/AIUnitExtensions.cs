﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public static class AIUnitExtensions  {
        public static float GetCurrentHealth(this IAIUnit aiUnit) { return aiUnit.SelectableWorldObject.GetCurrentHealth(); }
        public static void TakeDamage(this IAIUnit aiUnit,float damageAmount) { aiUnit.SelectableWorldObject.TakeDamage(damageAmount); }
        public static string ObjectName(this IAIUnit aiUnit) { return aiUnit.SelectableWorldObject.ObjectName; }
        public static cHealth HealthClass(this IAIUnit aiUnit) { return aiUnit.SelectableWorldObject.Health ; }       
    }
}

