﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public interface IAIUnit {
        SelectableWorldObject SelectableWorldObject { get; }
        void Tick();
    }
}

