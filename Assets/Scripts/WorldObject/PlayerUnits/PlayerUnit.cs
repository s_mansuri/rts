﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;
using DG.Tweening;

namespace RTS {
    [RequireComponent(typeof(NavMeshAgent))]
    public class PlayerUnit : SelectableWorldObject,IControllable,IPlayerUnit,IArmed {
        NavMeshAgent m_NavAgent;
        IWeapon m_Weapon;
        public GameObject m_WeaponPrefab;
        public Transform m_WeaponParent;
        DiContainer m_Container;
        Ray rayToTarget;
        IHealth m_CurrentTarget = null;

        bool m_AttackMode = false;
        bool m_TurningToFaceTarget = false;
        bool m_FacingTarget = false;
        PlayerUnitDeathSignal.Trigger m_DeathTrigger;
        [Inject] 
        void InitializePlayerUnit(DiContainer container,PlayerUnitDeathSignal.Trigger deathTrigger) {
            m_Container = container;
            CreateWeapon();
            if(m_Weapon == null)
                Debug.LogError(this.ObjectName+ " Unit needs a weapon");
            m_NavAgent = GetComponent<NavMeshAgent>();
            m_DeathTrigger = deathTrigger;
        }

        void CreateWeapon() {
            if(m_WeaponPrefab == null) {
                Debug.LogError("This unit(" + this.ObjectName + ") requires a weapon prefab");
            }

            m_Weapon = m_Container.InstantiatePrefab(m_WeaponPrefab).GetComponent<IWeapon>();
            m_Weapon.Transform.parent = m_WeaponParent;
            m_Weapon.Transform.localPosition= Vector3.zero;

        }


        public override void Tick() {
            if(m_NavAgent.velocity.sqrMagnitude > 1)
                m_Health.AlignHealthBarToCamera();

            if(InAttackMode)
                ExecuteAttackStrategy();

            Weapon.Tick();
            base.Tick();
        }

        public void FixedTick() {
           Weapon.FixedTick();
        }

        public void Patrol(Vector3[] Positions) {
            throw new NotImplementedException();
        }

        public void MoveToPosition(Vector3 position) {
            if(InAttackMode)
                BePassive();
            
            m_NavAgent.SetDestination(position);
            
        }

        public void AttackTarget(IHealth target,Vector3? worldPosition) {
            if(target == null && worldPosition == null) {
                Debug.LogError("Invalid Attack Target");
                return;
            }
            m_NavAgent.ResetPath();
            InAttackMode = true;
            m_CurrentTarget = target;
            

        }

        void ExecuteAttackStrategy() {
            if(!m_CurrentTarget.IsAlive()) {
                BePassive();
                return;
            }

            if(!PathToTargetIsClear(m_CurrentTarget)) {
                SeekTarget();
            }
            else {
                FaceTargetAndFire();
            }
        }

        void FaceTargetAndFire() {
            if(!FacingTarget()) {
                myTransform.DOLookAt(m_CurrentTarget.Transform.position,50f,AxisConstraint.Y,Vector3.up).SetSpeedBased().SetEase(Ease.Linear).OnComplete(FireAtTarget);
                m_Health.AlignHealthBarToCamera();
            }
                
            else
                FireAtTarget();
        }

        void FireAtTarget() {
            m_NavAgent.Stop();
            if(m_CurrentTarget != null) {
                
                m_Weapon.UseWeapon(m_CurrentTarget);
            }
        }

        void BePassive() {
            InAttackMode = false;
            m_CurrentTarget = null;
            myTransform.DOKill();
            m_NavAgent.ResetPath();
            Weapon.Reload();
        }

        void SeekTarget() {
            if(InAttackMode && m_NavAgent.velocity.sqrMagnitude == 0) {
                m_NavAgent.SetDestination(m_CurrentTarget.Transform.position);
                
            }
                
        }

        bool PathToTargetIsClear(IHealth target) {
            rayToTarget = new Ray(myTransform.position,target.Transform.position - myTransform.position);
            
            int layerMask = LayerMask.GetMask("Wall","AIUnit");
            RaycastHit[] hits = Physics.RaycastAll(rayToTarget,m_Weapon.Projectile.Range,layerMask);
            if(hits.Length == 0)
                return false;
            else {//check if there is a wall closer to us than the target
                float sqrDistanceToTarget = (target.Transform.position - myTransform.position).sqrMagnitude;
                foreach(RaycastHit hit in hits) {
                    if(hit.transform.tag == "Obstacle") {
                        float sqrDistanceToObstacle = (hit.transform.position - myTransform.position).sqrMagnitude;
                        if(sqrDistanceToObstacle < sqrDistanceToTarget)
                            return false;
                    }
                }
                return true;
            }
            
        }

        

        bool FacingTarget() {
            Vector3 targetDir = m_CurrentTarget.Transform.position - transform.position;
            float angle = Vector3.Angle(targetDir,transform.forward);

            if(angle < 1f)
                return true;
            else
                return false;
        }

        bool InAttackMode {
            get { return m_AttackMode; }
            set { m_AttackMode = value; }
        }

        void OnDrawGizmos() {
            Gizmos.DrawRay(rayToTarget);
        }

        public override void HandleDeath() {
            m_DeathTrigger.Fire(this);
            base.HandleDeath();
        }

        public IControllable Controllable {
            get {
                return this;
            }
        }

        public NavMeshAgent NavigationAgent {
            get {
                return m_NavAgent;
            }
        }

        public ISelectable Selectable {
            get {
                return this;
            }
        }

        [SerializeField]
        public IWeapon Weapon {
            get {
                if(m_Weapon == null)
                    CreateWeapon();
                return m_Weapon;
            }

            set {
                m_Weapon = value;
            }
        }

        public class PlayerUnitDeathSignal : Signal<IPlayerUnit>{
            public class Trigger : TriggerBase { }
        }

    }
}

