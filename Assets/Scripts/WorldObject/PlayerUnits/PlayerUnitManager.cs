﻿using UnityEngine;
using System.Collections;
using Zenject;
using System.Collections.Generic;
using System;

namespace RTS {
    public class PlayerUnitManager:ITickable,IDisposable,IInitializable,IFixedTickable {

        
        List<IPlayerUnit> m_AllPlayerUnits;
        //>
        List<IPlayerUnit> m_CurrentSelection;

        Camera m_MainCamera;

        WorldObjectsManager _worldManager;
        

        public PlayerUnitManager(cInput.LeftMouseClickedOrDragged selection,WorldObjectsManager worldObjectManager, 
            [Inject(Id =GameInstaller.Cameras.Main)] Camera mainCam) {
                m_MainCamera = mainCam;
                _worldManager = worldObjectManager; 
        }


        public void Initialize() {
            Debug.Log("Player Units Initialized");
            m_CurrentSelection = new List<IPlayerUnit>();
            m_AllPlayerUnits = _worldManager.AllPlayerUnitsList;
            
        }

        public void Dispose() {
           
        }

        public void Tick() {
            if(m_AllPlayerUnits!=null && m_AllPlayerUnits.Count>0)
                foreach(IPlayerUnit unit in m_AllPlayerUnits)
                    unit.Tick();
        }

        public void FixedTick() {
            if(m_AllPlayerUnits != null && m_AllPlayerUnits.Count > 0)
                foreach(IPlayerUnit unit in m_AllPlayerUnits)
                    unit.FixedTick();
        }

        public void BoxSelectUnits(Vector2 startPoint, Vector2 deltaSize, bool additiveSelection = false) {
            
            foreach(IPlayerUnit unit in m_AllPlayerUnits) {

                if(unit.Selectable.IsSelected()) {
                   
                    if(additiveSelection || unit.Selectable.SelectIfWithinBounds(startPoint,deltaSize))//if the unit falls under the selection area or was already selected
                        continue;
                    else
                        DeSelectUnit(unit);
                }
                else if(unit.Selectable.SelectIfWithinBounds(startPoint,deltaSize)) {
                   
                    SelectUnit(unit);
                    
                }
                
            }


            //PrintSelectionSize();
        }

        public void SingleSelectUnit(Vector2 screenPoint,bool additiveSelection = false) {
            if(!additiveSelection)
                ClearCurrentSelection();
            

            Ray ray = m_MainCamera.ScreenPointToRay(screenPoint);
            RaycastHit hit;
            LayerMask layerMask = LayerMask.GetMask("Unit");
            if(Physics.Raycast(ray,out hit,Mathf.Infinity,layerMask)) {
                IPlayerUnit unit = hit.transform.GetComponentInParent<PlayerUnit>();
                if(unit == null)
                    unit = hit.transform.GetComponentInChildren<PlayerUnit>();
                if(unit == null) {
                    Debug.LogError("CAn't find PlayerUnit class in object hit");
                    return;
                }

                if(unit.Selectable.IsSelected()) {
                    DeSelectUnit(unit);
                }
                else {
                    SelectUnit(unit);
                }

            }

            //PrintSelectionSize();
            
        }

        void SelectUnit(IPlayerUnit unit) {
            m_CurrentSelection.Add(unit);
            unit.Selectable.SetSelected();
            PrintSelectionSize("Selected: ");
        }

        void DeSelectUnit(IPlayerUnit unit) {
            m_CurrentSelection.Remove(unit);
            unit.Selectable.Deselect();
            PrintSelectionSize("Deselected: ");
            
        }

        void ClearCurrentSelection() {
            foreach(IPlayerUnit unit in m_CurrentSelection)
                unit.Selectable.Deselect();
            m_CurrentSelection.Clear();
            PrintSelectionSize("Clear: ");
            
        }

        public void SetSelectedUnitsDestination(Vector3 worldPoint) {
            if(m_CurrentSelection.Count == 0)
                return;
            foreach( IPlayerUnit unit in m_CurrentSelection) {
               
                unit.Controllable.MoveToPosition(worldPoint);
            }
        }

        public void SetSlectedUnitsTarget(IHealth target, Vector3? worldPoint =null) {
            if(m_CurrentSelection.Count == 0)
                return;
            foreach(IPlayerUnit unit in m_CurrentSelection)
                unit.Controllable.AttackTarget(target,worldPoint);
        }

        public List<IPlayerUnit> AllUnitsList {
            get {
                return m_AllPlayerUnits;
            }
            set {
                if(m_AllPlayerUnits == null)
                    m_AllPlayerUnits = new List<IPlayerUnit>();
                m_AllPlayerUnits = value;
            }
        }

        public List<IPlayerUnit> CurrentSelection {
            get {
                return m_CurrentSelection;
            }
        }

        public bool IsAnyUnitSelected {
            get {
                return m_CurrentSelection.Count>0;
            }
        }

        void PrintSelectionSize(string message="") {
           // Debug.Log(message+"Now you have " + ((m_CurrentSelection.Count == 1) ? "1 unit" : m_CurrentSelection.Count + " units")+ " selected");

        }

        
    }
}

