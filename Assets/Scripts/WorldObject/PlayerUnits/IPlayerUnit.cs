﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public interface IPlayerUnit {
        IControllable Controllable { get;}
        ISelectable Selectable { get; }
        void Tick();
        void FixedTick();
        Transform Transform { get; }
    }
}

