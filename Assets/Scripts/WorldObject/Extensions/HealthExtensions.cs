﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public static class HealthExtensions {

        public static float GetCurrentHealth(this IHealth worldObject) {
            return worldObject.Health.GetHealth();
        }

        public static float GetMaxHealth(this IHealth worldObject) {
            return worldObject.Health.GetMaxHealth();
        }

        public static void UpdateDisplay(this IHealth worldObject) {
            worldObject.Health.UpdateDisplay();
        }

        public static bool IsAlive(this IHealth worldObject) {
            return worldObject.Health.IsAlive();
        }

        public static void AlignHealthBar(this IHealth worldObject, bool instantly = false) {
            worldObject.Health.AlignHealthBarToCamera(instantly);
        }

        public static void EnableHealthBar(this IHealth worldObject) {
            worldObject.Health.EnableViewDueToSelection();
        }

        public static void DisableHealthBar(this IHealth worldObject) {
            worldObject.Health.DisableVIewDueToDeSelection();
        }

        public static void TakeDamage(this IHealth worldObject,float damageAmount) {
            worldObject.Health.TakeDamage(damageAmount);
        }

       
    }
}
