﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public interface IControllable {
        void MoveToPosition(Vector3 position);
        void AttackTarget(IHealth target, Vector3? worldPosition);
        void Patrol(Vector3[] Positions);
        NavMeshAgent NavigationAgent { get; }
    }
}

