﻿using UnityEngine;
using System.Collections;
namespace RTS {
    public interface IDisplayable {

        void EnableDisplayables();
        void DisableDisplayables();
    }
}

