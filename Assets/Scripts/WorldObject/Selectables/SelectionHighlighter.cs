﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public class SelectionHighlighter {
        
        Material[] m_Materials;
        Color[] m_OriginalMaterialColors;
        IDisplayable[] m_Displayables;

        public SelectionHighlighter(GameObject parent, params IDisplayable[] displayables) {
            var meshRenderers = parent.GetComponentsInChildren<MeshRenderer>();
            m_Materials = new Material[meshRenderers.Length];
            m_OriginalMaterialColors = new Color[m_Materials.Length];
            for(int i = 0;i < meshRenderers.Length;i++) {
                m_Materials[i] = meshRenderers[i].material;
                m_OriginalMaterialColors[i] = m_Materials[i].color;

            }

            m_Displayables = displayables;
        }

        public void UpdateVisuals(bool selected) {
            for(int i = 0;i < m_Materials.Length;i++) {
                m_Materials[i].color = selected ? Color.green : m_OriginalMaterialColors[i];
            }

            for(int i = 0;i < m_Displayables.Length;i++) {
                if(selected) {
                   
                    m_Displayables[i].EnableDisplayables();
                }
                    
                else {
                   
                    m_Displayables[i].DisableDisplayables();
                }
                    
            }
                
        }
    }
}

