﻿using UnityEngine;
using System.Collections;
using System;

using Zenject;
namespace RTS {
    public class SelectableWorldObject : WorldObject, ISelectable {

        SelectionStateManager m_SelectionStateManager;

        [Inject]
        public void Construct() {
            m_SelectionStateManager = new SelectionStateManager(this,this);
            //Debug.Log("Selectable world object Initialized");

        }

        public override void Tick() {
            m_SelectionStateManager.Tick();

            base.Tick();
        }


        public SelectionStateManager SelectionStateManager {
            get {
                return m_SelectionStateManager;
            }
        }

        public Camera MainCamera {
            get {
                return m_MainCamera;
            }
        }


    }
}

