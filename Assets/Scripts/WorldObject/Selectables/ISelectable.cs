﻿using UnityEngine;
using System.Collections;

namespace RTS {
    public interface ISelectable {
        //bool Selected { get; set; }
        //bool Preselected { get; set; }
       
        //bool SelectIfWithinBounds(Bounds selection);
        Transform Transform { get; }
        SelectionStateManager SelectionStateManager { get; }
        Camera MainCamera { get; }
        void DestroyObject();
    }
}