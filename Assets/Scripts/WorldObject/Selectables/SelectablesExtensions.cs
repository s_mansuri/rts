﻿using UnityEngine;
using System.Collections;

namespace RTS {

    public static class SelectablesExtensions {
        public static bool SelectIfWithinBounds(this ISelectable worldObject, Bounds bounds) {
            Vector3 worldObjectViewPos = worldObject.MainCamera.WorldToViewportPoint(worldObject.Transform.position);
            return worldObject.SelectionStateManager.SelectIfWithinBounds(bounds,worldObjectViewPos);
        }

        public static bool SelectIfWithinBounds(this ISelectable worldObject, Vector2 startPoint, Vector2 deltaSize) {
            var viewPortBounds = worldObject.MainCamera.GetViewPortBounds(startPoint,startPoint + deltaSize);
            return worldObject.SelectIfWithinBounds(viewPortBounds);
        }

        

        public static void SetSelected(this ISelectable selectableObject) {    
            selectableObject.SelectionStateManager.SetSelected();
        }
        
        public static void Deselect(this ISelectable selectableObject) {
            selectableObject.SelectionStateManager.Deselect();
        }

        /// <summary>
        /// Switches between being selected and deselected. Returns whether the unit is selected at the end of the operation
        /// </summary>
        /// <param name="selectableObject"></param>
        /// <returns></returns>
        public static bool ToggleSelect(this ISelectable selectableObject) {
            if(selectableObject.SelectionStateManager.IsSelected) {
                selectableObject.SelectionStateManager.Deselect();
                return false;
            }
            else {
                selectableObject.SelectionStateManager.SetSelected();
                return true;
            }
                
        }

        public static bool IsSelected(this ISelectable selectableObject) {
            return selectableObject.SelectionStateManager.IsSelected;
        }

        
        
    }
}

