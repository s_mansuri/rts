﻿using UnityEngine;
using System.Collections;
namespace RTS{

    public class SelectionStateManager {
        bool m_Selected = false;
        bool m_PreSelected = false;
        bool m_VisualStateUpdated = false;
        Bounds m_WorldBounds;
        ISelectable m_Parent;
        SelectionHighlighter m_selectionHighlighter;

        public SelectionStateManager(ISelectable parent, params IDisplayable[] displayables) {
            if(displayables != null)
                m_selectionHighlighter = new SelectionHighlighter(parent.Transform.gameObject, displayables);
            //m_Bounds = parent.ScreenBounds();
            m_WorldBounds = ((WorldObject)parent).GetTotalBounds();
            m_Parent = parent;

        }

        public bool SelectIfWithinBounds(Bounds selection,Vector3 viewportPosition) {
            if(selection.Contains(viewportPosition)) {
                if(!m_Selected)//only update visuals if not already selected
                    m_VisualStateUpdated = false;

                m_Selected = true;
                return true;
            }
            else {
                Deselect();
                return false;
            }
        }

       public void SetSelected() {
            if(m_Selected!=true)//only update visuals if this unit was not previously Selected
                m_VisualStateUpdated = false;
            m_Selected = true;
            
       }

        public void Deselect() {
            if(m_Selected == true)//only update visuals if this unit was previously Selected
                m_VisualStateUpdated = false;
            m_Selected = false;
            
        }

        public void Tick() {
            if(!m_VisualStateUpdated && m_selectionHighlighter!=null) {
                m_selectionHighlighter.UpdateVisuals(m_Selected);
                m_VisualStateUpdated = true;
            }
        }

        public bool IsSelected { get { return m_Selected; } set { m_Selected = value; } }
        bool Preselected { get { return m_PreSelected; } set { m_PreSelected = value; } }

        
    }
}

