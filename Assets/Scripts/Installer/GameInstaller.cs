﻿using UnityEngine;
using System.Collections;
using Zenject;
using System;

namespace RTS {
    

    public class GameInstaller : MonoInstaller {
        [SerializeField]
        Settings _settings;

        public enum Cameras {
            Main
        }

        public override void InstallBindings() {
            BindClasses();
            BindSettings();
            BindSignals();
            SetExecutionOrder();

        }

       

        void BindClasses() {
            Container.BindAllInterfacesAndSelf<WorldObjectsManager>().To<WorldObjectsManager>().AsSingle();
            Container.BindAllInterfacesAndSelf<PlayerUnitManager>().To<PlayerUnitManager>().AsSingle();
            Container.BindAllInterfacesAndSelf<cInput>().To<cInput>().AsSingle();
            Container.BindAllInterfacesAndSelf<cPlayer>().To<cPlayer>().AsSingle();
        }

        void BindSettings() {
            Container.BindInstance(_settings.MainCamera).WithId(Cameras.Main);
            Container.BindInstance(_settings.UIHook).AsSingle();
        }

        void BindSignals() {
            Container.BindSignal<cInput.LeftMouseClickedOrDragged>();
            Container.BindTrigger<cInput.LeftMouseClickedOrDragged.Trigger>();
            Container.BindSignal<cInput.RightMouseClicked>();
            Container.BindTrigger<cInput.RightMouseClicked.Trigger>();
            Container.BindSignal<cInput.MutliSelect>();
            Container.BindTrigger<cInput.MutliSelect.Trigger>();

            Container.BindSignal<PlayerUnit.PlayerUnitDeathSignal>();
            Container.BindTrigger<PlayerUnit.PlayerUnitDeathSignal.Trigger>();

            Container.BindSignal<AIUnit.AIUnitDeathSignal>();
            Container.BindTrigger<AIUnit.AIUnitDeathSignal.Trigger>();
        }
        void SetExecutionOrder() {
            Container.BindInitializableExecutionOrder<WorldObjectsManager>(-1);  
        }

        [Serializable]
        public class Settings {
           

            public Camera MainCamera;
            public UIHook UIHook;
        }
    }

    
}
